package com.imilka.zimadtest.models;

import java.util.List;

public class QueryResponse {

    private String message;
    private List<Picture> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Picture> getData() {
        return data;
    }

    public void setData(List<Picture> data) {
        this.data = data;
    }
}
