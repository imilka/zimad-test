package com.imilka.zimadtest.models;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.imilka.zimadtest.net.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PictureList {

    private MutableLiveData<List<Picture>> mPictures = new MutableLiveData<>();

    public MutableLiveData<List<Picture>> getPictures() {
        return mPictures;
    }

    public void fetchPictures(String query) {
        Callback<QueryResponse> callback = new Callback<QueryResponse>() {
            @Override
            public void onResponse(Call<QueryResponse> call, Response<QueryResponse> response) {
                QueryResponse body = response.body();
                mPictures.setValue(body.getData());
            }

            @Override
            public void onFailure(Call<QueryResponse> call, Throwable t) {
                Log.e("FetchPictures", t.getMessage(), t);
            }
        };

        ApiService.getInstance().getApi().pictureList(query).enqueue(callback);
    }
}
