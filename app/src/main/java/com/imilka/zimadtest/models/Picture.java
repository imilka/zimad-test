package com.imilka.zimadtest.models;

import java.io.Serializable;

public class Picture implements Serializable {

    private String url;
    private String title;

    public String getUrl() {
        return url.replace("http", "https");
    }

    public String getTitle() {
        return title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

