package com.imilka.zimadtest.net;

import com.imilka.zimadtest.models.QueryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("api.php")
    Call<QueryResponse> pictureList(@Query("query") String query);

}
