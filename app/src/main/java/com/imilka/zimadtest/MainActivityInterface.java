package com.imilka.zimadtest;

import com.imilka.zimadtest.models.Picture;

public interface MainActivityInterface {
    void openPictureDetailsFragment(Picture picture, int position);
}
