package com.imilka.zimadtest.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.imilka.zimadtest.databinding.PictureItemBinding;
import com.imilka.zimadtest.models.Picture;

public class PictureViewHolder extends RecyclerView.ViewHolder {
    private final PictureItemBinding mBinding;
    private final PicturesAdapter.IClickListener mClickListener;

    PictureViewHolder(PictureItemBinding binding, PicturesAdapter.IClickListener clickListener) {
        super(binding.getRoot());
        this.mBinding = binding;
        mClickListener = clickListener;
    }

    void bind(Picture picture, int position) {
        mBinding.setPosition(position + 1);
        mBinding.setViewHolder(this);
        mBinding.setPicture(picture);
        mBinding.executePendingBindings();
    }

    public void onClick() {
        if (mClickListener == null) {
            return;
        }
        mClickListener.onItemClick(getAdapterPosition());
    }
}