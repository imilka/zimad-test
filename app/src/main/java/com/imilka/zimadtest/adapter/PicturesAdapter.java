package com.imilka.zimadtest.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imilka.zimadtest.databinding.PictureItemBinding;
import com.imilka.zimadtest.models.Picture;

import java.util.List;

public class PicturesAdapter extends RecyclerView.Adapter<PictureViewHolder> {

    private List<Picture> mPictures;
    private IClickListener mClickListener;

    public interface IClickListener {
        void onItemClick(int position);
    }

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        PictureItemBinding itemBinding = PictureItemBinding.inflate(layoutInflater, parent, false);
        return new PictureViewHolder(itemBinding, mClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureViewHolder holder, int position) {
        Picture picture = mPictures.get(position);
        holder.bind(picture, position);
    }

    @Override
    public int getItemCount() {
        return mPictures == null ? 0 : mPictures.size();
    }

    public void setPictures(List<Picture> pictures) {
        this.mPictures = pictures;
    }

    public void setClickListener(IClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }
}
