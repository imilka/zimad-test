package com.imilka.zimadtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.imilka.zimadtest.databinding.MainActivityBinding;
import com.imilka.zimadtest.models.Picture;
import com.imilka.zimadtest.view.PictureFragment;
import com.imilka.zimadtest.view.QueryResultsFragment;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements MainActivityInterface {

    private static final String EXTRA_TAB_POSITION = "SelectedTabPosition";
    private static final String EXTRA_TAB_VISIBILITY = "TabLayoutVisibility";
    private static final String TAG_PICTURE_DETAILS = "TagPictureDetails";

    private TabLayout mTabLayout;
    private int mCurrentPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivityBinding activityBinding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        mTabLayout = activityBinding.tabs;

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setupFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (savedInstanceState == null) {
            setupFragment(0);
        } else {
            mTabLayout.setVisibility(savedInstanceState.getInt(EXTRA_TAB_VISIBILITY));

            mCurrentPosition = savedInstanceState.getInt(EXTRA_TAB_POSITION);
            TabLayout.Tab currentTab = mTabLayout.getTabAt(mCurrentPosition);
            if (currentTab != null) {
                currentTab.select();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_TAB_POSITION, mTabLayout.getSelectedTabPosition());
        outState.putInt(EXTRA_TAB_VISIBILITY, mTabLayout.getVisibility());
    }

    private void setupFragment(int position) {
        if (mCurrentPosition == position) {
            return;
        }

        String query = getQueryForPosition(position);
        Fragment cachedFragment = getSupportFragmentManager().findFragmentByTag(query);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, cachedFragment != null ? cachedFragment : QueryResultsFragment.newInstance(query), query)
                .addToBackStack(query)
                .commit();

        mCurrentPosition = position;
    }

    private String getQueryForPosition(int position) {
        return position == 0 ? "cat" : "dog";
    }

    public void openPictureDetailsFragment(Picture picture, int position) {
        if (getSupportFragmentManager().findFragmentByTag(TAG_PICTURE_DETAILS) == null) {
            PictureFragment pictureFragment = PictureFragment.newInstance(picture, position);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, pictureFragment, TAG_PICTURE_DETAILS)
                    .addToBackStack(null)
                    .commit();
            mTabLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        mTabLayout.setVisibility(View.VISIBLE);
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        }

        boolean poppedQueryResultsFragment = getSupportFragmentManager().popBackStackImmediate(getQueryForPosition(mCurrentPosition), 0);
        if (!poppedQueryResultsFragment) {
            finish();
        }
    }
}
