package com.imilka.zimadtest.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.imilka.zimadtest.adapter.PicturesAdapter;
import com.imilka.zimadtest.models.Picture;
import com.imilka.zimadtest.models.PictureList;

import java.util.List;

public class QueryResultsViewModel extends ViewModel implements PicturesAdapter.IClickListener {

    private PicturesAdapter mAdapter;
    private PictureList mPictureList;
    private MutableLiveData<SelectedPictureData> mSelectedPicture;

    public class SelectedPictureData {
        public Picture picture;
        public int position;

        SelectedPictureData(Picture picture, int position) {
            this.picture = picture;
            this.position = position;
        }
    }

    public void initialize() {
        mAdapter = new PicturesAdapter();
        mAdapter.setClickListener(this);
        mPictureList = new PictureList();
        mSelectedPicture = new MutableLiveData<>();
    }

    public MutableLiveData<List<Picture>> getPictures() {
        return mPictureList.getPictures();
    }

    public PicturesAdapter getAdapter() {
        return mAdapter;
    }

    public void updatePicturesList(List<Picture> pictures) {
        mAdapter.setPictures(pictures);
        mAdapter.notifyDataSetChanged();
    }

    public void reloadData(String query) {
        mPictureList.fetchPictures(query);
    }

    private Picture getPictureAt(int index) {
        return mPictureList.getPictures().getValue() != null && index < mPictureList.getPictures().getValue().size() ?
                mPictureList.getPictures().getValue().get(index) :
                null;
    }

    @Override
    public void onItemClick(int position) {
        Picture picture = getPictureAt(position);
        mSelectedPicture.setValue(new SelectedPictureData(picture, position));
    }

    public MutableLiveData<SelectedPictureData> getSelectedPicture() {
        return mSelectedPicture;
    }

    public void resetSelectedPicture() {
        mSelectedPicture.setValue(null);
    }
}
