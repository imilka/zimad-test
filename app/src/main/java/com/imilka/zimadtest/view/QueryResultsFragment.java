package com.imilka.zimadtest.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imilka.zimadtest.MainActivityInterface;
import com.imilka.zimadtest.R;
import com.imilka.zimadtest.adapter.PicturesAdapter;
import com.imilka.zimadtest.databinding.MainFragmentBinding;
import com.imilka.zimadtest.models.Picture;
import com.imilka.zimadtest.viewmodel.QueryResultsViewModel;

public class QueryResultsFragment extends Fragment {

    private static final String EXTRA_QUERY = "Query";

    private QueryResultsViewModel mViewModel;
    private RecyclerView mPicturesList;
    private String mQuery;

    public QueryResultsFragment() {

    }

    public static QueryResultsFragment newInstance(String query) {
        QueryResultsFragment f = new QueryResultsFragment();
        Bundle bundle = new Bundle(1);
        bundle.putString(EXTRA_QUERY, query);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuery = getArguments() != null ? getArguments().getString(EXTRA_QUERY) : "cat";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        MainFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false);
        boolean isFragmentCached = mViewModel != null;

        if (!isFragmentCached) {
            mViewModel = ViewModelProviders.of(this).get(QueryResultsViewModel.class);

            if (savedInstanceState == null) {
                mViewModel.initialize();
            }
        }

        mPicturesList = binding.picturesList;
        setupRecyclerView();
        setupClickListener();

        if (!isFragmentCached && savedInstanceState == null) {
            setupDataReload();
            mViewModel.reloadData(mQuery);
        }

        return binding.getRoot();
    }

    private void setupRecyclerView() {
        mPicturesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mPicturesList.setHasFixedSize(true);
        mPicturesList.setAdapter(mViewModel.getAdapter());
    }

    private void setupClickListener() {
        mViewModel.getSelectedPicture().observe(this, picture -> {
            if (picture == null) {
                return;
            }

            openPictureDetails(picture.picture, picture.position);
            mViewModel.resetSelectedPicture();
        });
    }

    private void setupDataReload() {
        mViewModel.getPictures().observe(this, pictures -> mViewModel.updatePicturesList(pictures));
    }

    // i'm sure this is not the best way to do this
    private void openPictureDetails(Picture picture, int position) {
        MainActivityInterface mainActivityInterface = ((MainActivityInterface) getActivity());
        if (mainActivityInterface != null) {
            mainActivityInterface.openPictureDetailsFragment(picture, position);
        }
    }
}
