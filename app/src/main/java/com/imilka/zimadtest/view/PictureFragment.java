package com.imilka.zimadtest.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imilka.zimadtest.R;
import com.imilka.zimadtest.databinding.PictureFragmentBinding;
import com.imilka.zimadtest.models.Picture;

public class PictureFragment extends Fragment {

    private static final String EXTRA_PICTURE = "Picture";
    private static final String EXTRA_POSITION = "Position";

    private Picture mPicture;
    private int mPosition;

    public PictureFragment() {

    }

    public static PictureFragment newInstance(Picture picture, int position) {
        PictureFragment fragment = new PictureFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_PICTURE, picture);
        args.putInt(EXTRA_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPicture = (Picture) getArguments().getSerializable(EXTRA_PICTURE);
            mPosition = getArguments().getInt(EXTRA_POSITION);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        PictureFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.picture_fragment, container, false);
        binding.setPosition(mPosition + 1);
        binding.setPicture(mPicture);
        return binding.getRoot();
    }
}
